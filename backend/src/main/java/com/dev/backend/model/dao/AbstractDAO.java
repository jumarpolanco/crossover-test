/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.model.dao;

import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Jumar
 * @param <T>
 */
public abstract class AbstractDAO<T> {

    private final Class entityClass;

    public AbstractDAO(Class entityClass) {
        this.entityClass = entityClass;
    }

    public abstract Session getSession();

    public T save(T entity){
        try {
            getSession().beginTransaction();
            getSession().save(entity);
            getSession().getTransaction().commit();
        } catch (Exception e) {
            getSession().getTransaction().rollback();
            e.printStackTrace();
        } finally {
            getSession().close();
        }
        return entity;
    }

    public T edit(T entity) {
        try {
            getSession().beginTransaction();
            getSession().saveOrUpdate(entity);
            getSession().getTransaction().commit();
        } catch (Exception e) {
            getSession().getTransaction().rollback();
            e.printStackTrace();
        } finally {
            getSession().close();
        }
        return entity;
    }

    public void remove(T entity) {
        try {
            getSession().beginTransaction();
            getSession().delete(this);
            getSession().getTransaction().commit();
        } catch (Exception e) {
            getSession().getTransaction().rollback();
            e.printStackTrace();
        } finally {
            getSession().close();
        }
    }

    public List<T> findAll() {
        List<T> result = null;
        try {
            result = getSession().createCriteria(entityClass).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            getSession().close();
        }
        return result;
    }

    public T findById(String id) {
        T result = null;
        try {
            result = (T) getSession().get(entityClass, id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            getSession().close();
        }
        return result;
    }
}
