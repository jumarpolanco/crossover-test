/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.model.dao;

import com.dev.backend.model.entities.OrderDetails;
import com.dev.backend.util.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author Jumar
 */
public class OrderDetailsDAO extends AbstractDAO<OrderDetails>{

    public OrderDetailsDAO() {
        super(OrderDetails.class);
    }   
    

    @Override
    public Session getSession() {
        return HibernateUtil.getSession();
    }
    
}
