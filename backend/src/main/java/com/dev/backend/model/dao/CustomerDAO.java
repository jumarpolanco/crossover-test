/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.model.dao;

import com.dev.backend.model.entities.Customer;
import com.dev.backend.util.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author Jumar
 */
public class CustomerDAO extends AbstractDAO<Customer> {

    public CustomerDAO() {
        super(Customer.class);
    }    
    

    @Override
    public Session getSession() {
        return HibernateUtil.getSession();
    }
    
}
