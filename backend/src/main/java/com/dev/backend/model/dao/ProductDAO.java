/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.model.dao;

import com.dev.backend.model.entities.Product;
import com.dev.backend.util.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author Jumar
 */
public class ProductDAO extends AbstractDAO<Product> {

    public ProductDAO() {
        super(Product.class);
    }

    @Override
    public Session getSession() {
        return HibernateUtil.getSession();
    }

}
