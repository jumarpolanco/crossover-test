/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.model.dao;

import com.dev.backend.model.entities.Customer;
import com.dev.backend.model.entities.OrderDetails;
import com.dev.backend.model.entities.SalesOrder;
import com.dev.backend.util.CustomerLimitException;
import com.dev.backend.util.HibernateUtil;
import com.dev.backend.util.OutOfStockException;
import org.hibernate.Session;

/**
 *
 * @author Jumar
 */
public class SalesOrderDAO extends AbstractDAO<SalesOrder> {

    private OrderDetailsDAO orderDAO;
    private ProductDAO productDAO;
    private CustomerDAO customerDAO;
            
    public SalesOrderDAO() {
        super(SalesOrder.class);
        orderDAO = new OrderDetailsDAO();
        productDAO = new ProductDAO();
        customerDAO = new CustomerDAO();
    }

    @Override
    public Session getSession() {
        return HibernateUtil.getSession();
    }
    
    
    
    public SalesOrder saveOrder(SalesOrder entity) throws Exception{
        if (validateCustomerLimits(entity)) {
            throw new CustomerLimitException();
        }
        if (validateStock(entity)) {
            throw new OutOfStockException();
        }
        
        super.save(entity);
        for (OrderDetails od : entity.getOrderDetails()) {
            od.setSalesOrder(entity);
            orderDAO.save(od);
        }
        return entity;
    }

    /*
     * This method validates if the inventory could fulfill the sales order
     */
    public boolean validateStock(SalesOrder salesOrder) {
        for (Object detail : salesOrder.getOrderDetails()) {//iterate the details and verify any one exceeds the stock
            OrderDetails det = (OrderDetails)detail;
            if (det.getQuantity() > productDAO.findById(det.getProduct().getCode()).getQuantity()) {
                return true;
            }
        }
        return false;
    }

    /*
     * This method validates if the sales order exceeds any customer limit
     */
    public boolean validateCustomerLimits(SalesOrder salesOrder) {
        Customer cus = customerDAO.findById(salesOrder.getCustomer().getCode());
        return salesOrder.getTotalPrice()
                .compareTo(// do the substraction between customer credit limit and customer current credit for having how much credit left
                        cus.getCreditLimit()
                                .subtract(cus.getCurrentCredit())
                ) == 1;//camparator returns 1 when first is bigger than second.
    }

}
