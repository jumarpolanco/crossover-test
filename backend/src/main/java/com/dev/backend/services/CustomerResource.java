/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.services;

import com.dev.backend.model.dao.CustomerDAO;
import com.dev.backend.model.entities.Customer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;

/**
 * REST Web Service
 *
 * @author Jumar
 */
@Path("Customer")
public class CustomerResource {

    @Context
    private UriInfo context;
    private CustomerDAO dao;

    /**
     * Creates a new instance of CustomerResource
     */
    public CustomerResource() {
        dao = new CustomerDAO();
    }
    
    @POST
    @Consumes("application/json")
    public void create(Customer entity) {
        try {
            dao.save(entity);
        } catch (Exception ex) {
            Logger.getLogger(ProductResource.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }

    @PUT
    @Consumes("application/json")
    public void edit(Customer entity) {
        dao.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(Customer entity) {
        dao.remove(entity);
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Customer find(@PathParam("id") String id) {
        return dao.findById(id);
    }

    @GET
    @Produces("application/json")
    public List<Customer> findAll() {
        return dao.findAll();
    }
}
