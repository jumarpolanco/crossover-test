/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.backend.services;

import com.dev.backend.model.dao.SalesOrderDAO;
import com.dev.backend.model.entities.OrderDetails;
import com.dev.backend.model.entities.SalesOrder;
import com.dev.backend.util.CustomerLimitException;
import com.dev.backend.util.OutOfStockException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.WebApplicationException;

/**
 * REST Web Service
 *
 * @author Jumar
 */
@Path("SalesOrder")
public class SalesOrderResource {

    @Context
    private UriInfo context;
    private SalesOrderDAO dao;

    /**
     * Creates a new instance of SalesOrderResource
     */
    public SalesOrderResource() {
        dao = new SalesOrderDAO();
    }

    @POST
    @Consumes("application/json")
    public void create(SalesOrder entity) {
        try {
            dao.saveOrder(entity);
        } catch (Exception ex) {
            if (ex instanceof CustomerLimitException) {
                throw new WebApplicationException(402);
            }
            if (ex instanceof OutOfStockException) {
                throw new WebApplicationException(404);
            }
            Logger.getLogger(ProductResource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @PUT
    @Consumes("application/json")
    public void edit(SalesOrder entity) {
        try {
            dao.saveOrder(entity);
        } catch (Exception ex) {
            if (ex instanceof CustomerLimitException) {
                throw new WebApplicationException(402);
            }
            if (ex instanceof OutOfStockException) {
                throw new WebApplicationException(404);
            }
            Logger.getLogger(ProductResource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(SalesOrder entity) {
        dao.remove(entity);
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public SalesOrder find(@PathParam("id") String id) {
        return dao.findById(id);
    }

    @GET
    @Produces("application/json")
    public List<SalesOrder> findAll() {
        return dao.findAll();
    }
}
