/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.frontend.services.entities;

import static com.dev.frontend.services.Utils.parseDouble;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jumar
 */
@XmlRootElement(name = "Product")
public class Product implements Serializable {

    private String code;
    private String description;
    private double price;
    private Integer quantity;

    public Product() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public static Product parseProduct(JsonObject json) {
        Product p = new Product();
        p.setCode(json.getString("code"));
        p.setDescription(json.getString("description"));
        p.setPrice(json.getJsonNumber("price").doubleValue());
        p.setQuantity(json.getJsonNumber("quantity").intValue());
        return p;
    }
    
    public static List<Product> parseProductList(JsonArray jsonArray){
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            products.add(parseProduct(jsonArray.getJsonObject(i)));
        }
        return products;
    }
}
