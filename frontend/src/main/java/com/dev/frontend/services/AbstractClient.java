/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.frontend.services;

import com.dev.frontend.services.providers.CustomJsonArrayProvider;
import com.dev.frontend.services.providers.CustomJsonProvider;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author Jumar
 * @param <T>
 */
public class AbstractClient<T>{

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI = "http://localhost:8080/backend/webresources";
    private final Class<T> responseType;

    public AbstractClient(Class<T> responseType) {
        this.responseType = responseType;
        // registering custom providers
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(CustomJsonProvider.class);
        clientConfig.register(CustomJsonArrayProvider.class);
        clientConfig.register(JacksonJsonProvider.class);
        //initialize rest client
        client = ClientBuilder.newClient(clientConfig);
        //construct the rest service path
        webTarget = client.target(BASE_URI).path(responseType.getSimpleName());
    }

    public Object edit(Object requestEntity) throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
    }

    public JsonObject find(String id) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{id}));
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(JsonObject.class);
    }

    public void create(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
    }

    public JsonArray findAll() throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(JsonArray.class);
    }

    public void remove(String id) throws ClientErrorException {
        webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request().delete();
    }

    public void close() {
        client.close();
    }
}
