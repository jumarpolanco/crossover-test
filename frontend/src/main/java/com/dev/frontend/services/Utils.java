package com.dev.frontend.services;

import com.dev.frontend.panels.ComboBoxItem;
import com.dev.frontend.services.entities.Customer;
import com.dev.frontend.services.entities.OrderDetails;
import com.dev.frontend.services.entities.Product;
import com.dev.frontend.services.entities.SalesOrder;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;

public class Utils
{
	public static Double parseDouble(String value)
	{
		if(value == null||value.isEmpty())
			return 0D;
		try
		{
			return Double.parseDouble(value);
		}
		catch(Exception e)
		{
			return 0D;
		}
	}
        /*
        * This method converts Customer list to a ComboBoxItem list
        */
        public static List<ComboBoxItem> customersToCombos(List<Customer> customers){
            List<ComboBoxItem> combos = new ArrayList<>();
            for (Customer customer : customers) {
                ComboBoxItem combo = new ComboBoxItem(customer.getCode(), customer.getName());
                combos.add(combo);
            }
            return combos;
        }
        /*
        * This method converts Product list to a ComboBoxItem list
        */
        public static List<ComboBoxItem> productsToCombos(List<Product> products){
            List<ComboBoxItem> combos = new ArrayList<>();
            for (Product product : products) {
                ComboBoxItem combo = new ComboBoxItem(product.getCode(), product.getDescription());
                combos.add(combo);
            }
            return combos;
        }       
}
