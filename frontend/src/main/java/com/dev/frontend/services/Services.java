package com.dev.frontend.services;

import java.util.ArrayList;
import java.util.List;

import com.dev.frontend.panels.ComboBoxItem;
import com.dev.frontend.services.entities.*;

public class Services {

    public static final int TYPE_PRODUCT = 1;
    public static final int TYPE_CUSTOMER = 2;
    public static final int TYPE_SALESORDER = 3;
    private static AbstractClient resourceClient;

    /*
     * This method calcules what kind of restful client needs to be implemented.
     */
    private static AbstractClient determineObjectType(int objectType) {
        switch (objectType) {
            case TYPE_CUSTOMER:
                return new AbstractClient<Customer>(Customer.class);
            case TYPE_PRODUCT:
                return new AbstractClient<Product>(Product.class);
            case TYPE_SALESORDER:
                return new AbstractClient<SalesOrder>(SalesOrder.class);
            //any new type of object just need to be mapped as an entity and added here with the abstract client
            //open to extension
            default:
                return null;
        }
    }

    public static Object save(Object object, int objectType) {
        resourceClient = determineObjectType(objectType);
        return resourceClient.edit(object);
    }

    public static Object readRecordByCode(String code, int objectType) {
        switch (objectType) {
            case TYPE_CUSTOMER:
                return Customer.parseCustomer(new AbstractClient<Customer>(Customer.class).find(code));
            case TYPE_PRODUCT:
                return Product.parseProduct(new AbstractClient<Product>(Product.class).find(code));
            case TYPE_SALESORDER:
                return SalesOrder.parseSalesOrder(new AbstractClient<SalesOrder>(SalesOrder.class).find(code));
            //any new type of object just need to be mapped as an entity and added here with the abstract client
            //open to extension
            default:
                return null;
        }
    }

    public static boolean deleteRecordByCode(String code, int objectType) {
        resourceClient = determineObjectType(objectType);
        resourceClient.remove(code);
        return true;
    }

    public static List listCurrentRecords(int objectType) {
        switch (objectType) {
            case TYPE_CUSTOMER:
                return Customer.parseCustomerList(new AbstractClient<Customer>(Customer.class).findAll());
            case TYPE_PRODUCT:
                return Product.parseProductList(new AbstractClient<Product>(Product.class).findAll());
            case TYPE_SALESORDER:
                return SalesOrder.parseSalesOrderList(new AbstractClient<SalesOrder>(SalesOrder.class).findAll());
            //any new type of object just need to be mapped as an entity and added here with the abstract client
            //open to extension
            default:
                return null;
        }
    }

    /*
     * This method is called when a Combo Box need to be initialized and should
     * return list of ComboBoxItem which contains code and description/name for all records of specified type
     */
    public static List<ComboBoxItem> listCurrentRecordRefernces(int objectType) {
        ArrayList<ComboBoxItem> comboList = new ArrayList<ComboBoxItem>();
        switch (objectType) {
            case TYPE_CUSTOMER:
                //look for all customers and send them to the converter util (customersToCombo) to be returned as the combo list
                comboList = (ArrayList<ComboBoxItem>) Utils.customersToCombos(Customer.parseCustomerList(new AbstractClient<Customer>(Customer.class).findAll()));
                break;
            case TYPE_PRODUCT:
                //look for all the products and send them to the converter util (productsToCombo) to be returned as the combo list
                comboList = (ArrayList<ComboBoxItem>) Utils.productsToCombos(Product.parseProductList(new AbstractClient<Product>(Product.class).findAll()));
                break;
            //any other combo conversion just need to be added here and in the util.
        }
        return comboList;
    }

    /*
     * This method is used to get unit price of product with the code passed as a parameter
     */
    public static double getProductPrice(String productCode) {
        return ((Product) readRecordByCode(productCode, TYPE_PRODUCT)).getPrice();
    }
}
