/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.frontend.services.entities;

import static com.dev.frontend.services.Utils.parseDouble;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jumar
 */
@XmlRootElement(name = "OrderDetails")
public class OrderDetails implements Serializable {

    private Integer number;
    private Integer quantity;
    private double unitPrice;
    private double totalPrice;
    private Product product;

    public OrderDetails() {
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public static OrderDetails parseOrderDetails(JsonObject json) {
        OrderDetails od = new OrderDetails();
        od.setNumber(json.getInt("number"));
        od.setProduct(Product.parseProduct(json.getJsonObject("product")));
        od.setQuantity(Integer.SIZE);
        od.setTotalPrice(json.getJsonNumber("totalPrice").doubleValue());
        od.setUnitPrice(json.getJsonNumber("unitPrice").doubleValue());
        return od;
    }

    public static List<OrderDetails> parseOrderDetailsList(JsonArray jsonArray) {
        List<OrderDetails> odList = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            odList.add(parseOrderDetails(jsonArray.getJsonObject(i)));
        }
        return odList;
    }

}
