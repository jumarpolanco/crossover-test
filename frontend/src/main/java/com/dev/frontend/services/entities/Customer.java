/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.frontend.services.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jumar
 */
@XmlRootElement(name = "Customer")
public class Customer implements Serializable {

    private String code;
    private String name;
    private String address;
    private String phone1;
    private String phone2;
    private double creditLimit;
    private double currentCredit;

    public Customer() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public double getCurrentCredit() {
        return currentCredit;
    }

    public void setCurrentCredit(double currentCredit) {
        this.currentCredit = currentCredit;
    }

    public static Customer parseCustomer(JsonObject json) {
        Customer c = new Customer();
        c.setCode(json.getString("code"));
        c.setName(json.getString("name"));
        c.setAddress(json.getString("address"));
        c.setPhone1(json.getString("phone1"));
        c.setPhone2(json.getString("phone2"));
        c.setCreditLimit(json.getJsonNumber("creditLimit").doubleValue());
        c.setCurrentCredit(json.getJsonNumber("currentCredit").doubleValue());
        return c;
    }
    
    public static List<Customer> parseCustomerList(JsonArray jsonArray){
        List<Customer> customers = new ArrayList<Customer>();
        for (int i = 0; i < jsonArray.size(); i++) {
            customers.add(parseCustomer(jsonArray.getJsonObject(i)));
        }
        return customers;
    }

}
