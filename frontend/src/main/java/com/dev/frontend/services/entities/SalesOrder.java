/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dev.frontend.services.entities;

import com.dev.frontend.services.Utils;
import static com.dev.frontend.services.Utils.parseDouble;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jumar
 */
@XmlRootElement(name = "SalesOrder")
public class SalesOrder implements Serializable {

    private String orderNumber;
    private double totalPrice;
    private Customer customer;
    private List<OrderDetails> orderDetails;

    public SalesOrder() {
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<OrderDetails> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public static SalesOrder parseSalesOrder(JsonObject json) {
        SalesOrder so = new SalesOrder();
        so.setCustomer(Customer.parseCustomer(json.getJsonObject("customer")));
        so.setOrderNumber(json.getString("orderNumber"));
        so.setTotalPrice(json.getJsonNumber("totalPrice").doubleValue());
        so.setOrderDetails(OrderDetails.parseOrderDetailsList(json.getJsonArray("orderDetails")));
        return so;
    }
    
    public static List<SalesOrder> parseSalesOrderList(JsonArray jsonArray){
        List<SalesOrder> salesOrders = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            salesOrders.add(parseSalesOrder(jsonArray.getJsonObject(i)));
        }
        return salesOrders;
    }
}
