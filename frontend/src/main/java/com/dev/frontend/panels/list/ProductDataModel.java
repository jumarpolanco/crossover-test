package com.dev.frontend.panels.list;

import java.util.List;

import com.dev.frontend.services.Services;
import com.dev.frontend.services.entities.Customer;
import com.dev.frontend.services.entities.Product;
import java.math.BigDecimal;

public class ProductDataModel extends ListDataModel {

    private static final long serialVersionUID = 7526529951747614655L;

    public ProductDataModel() {
        super(new String[]{"Code", "Description", "Price", "Quantity"}, 0);
    }

    @Override
    public int getObjectType() {
        return Services.TYPE_PRODUCT;
    }

    /*
     * This method use list returned by Services.listCurrentRecords and should convert it to array of rows
     * each row is another array of columns of the row
     */
    @Override
    public String[][] convertRecordsListToTableModel(List<Object> list) {
        String[][] productData = new String[list.size()][4];
        int index = 0;
        for (Object customerObj : list) {
            Product p = (Product) customerObj;
            productData[index][0] = p.getCode();
            productData[index][1] = p.getDescription();
            productData[index][2] = BigDecimal.valueOf(p.getPrice()).toPlainString();
            productData[index][3] = p.getQuantity().toString();
             index++;      
        }
        return productData;
    }
}
