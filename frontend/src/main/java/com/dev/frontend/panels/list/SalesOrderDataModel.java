package com.dev.frontend.panels.list;

import java.util.List;

import com.dev.frontend.services.Services;
import com.dev.frontend.services.entities.SalesOrder;

public class SalesOrderDataModel extends ListDataModel {

    private static final long serialVersionUID = 7526529951747614655L;

    public SalesOrderDataModel() {
        super(new String[]{"Order Number", "Customer", "Total Price"}, 0);
    }

    @Override
    public int getObjectType() {
        return Services.TYPE_SALESORDER;
    }

    /*
     * This method use list returned by Services.listCurrentRecords and should convert it to array of rows
     * each row is another array of columns of the row
     */
    @Override
    public String[][] convertRecordsListToTableModel(List<Object> list) {
	String[][] productData = new String[list.size()][3];
        int index = 0;
        for (Object customerObj : list) {
            SalesOrder so = (SalesOrder) customerObj;
            productData[index][0] = so.getOrderNumber();
            productData[index][1] = so.getCustomer().getName();
            productData[index][2] = Double.toString(so.getTotalPrice());
             index++;      
        }
        return productData;
    }
}
