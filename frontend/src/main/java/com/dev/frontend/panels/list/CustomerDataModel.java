package com.dev.frontend.panels.list;

import java.util.List;

import com.dev.frontend.services.Services;
import com.dev.frontend.services.entities.Customer;
import java.math.BigDecimal;

public class CustomerDataModel extends ListDataModel {

    private static final long serialVersionUID = 7526529951747613655L;

    public CustomerDataModel() {
        super(new String[]{"Code", "Name", "Phone", "Current Credit"}, 0);
    }

    @Override
    public int getObjectType() {
        return Services.TYPE_CUSTOMER;
    }

    /*
     * This method use list returned by Services.listCurrentRecords and should convert it to array of rows
     * each row is another array of columns of the row
     */
    @Override
    public String[][] convertRecordsListToTableModel(List<Object> list) {
        String[][] customerData = new String[list.size()][4];
        int index = 0;
        for (Object customerObj : list) {
            Customer c = (Customer) customerObj;
            customerData[index][0] = c.getCode();
            customerData[index][1] = c.getName();
            customerData[index][2] = c.getPhone1();
            customerData[index][3] = BigDecimal.valueOf(c.getCurrentCredit()).toPlainString();
             index++;      
        }
        return customerData;
    }
}
